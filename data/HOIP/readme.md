# Data description
This is a hybrid organic–inorganic perovskite (HOIP) crystal example where the goal is to
find the compound with the smallest inter-molecular binding energy. 

There are 3 sources of data (1 HF and 2 LFs). The first 3 comuns are categorical inputs and the last one is the output which is the inter-molecular binding energy.
