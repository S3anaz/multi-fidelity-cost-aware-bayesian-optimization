import torch
import numpy as np

def y_h(x, noise_std=0):

    y=0.6 *(x**4) - 0.3 * (x**3) - 3 * (x**2) + 2 * x

    if noise_std==0:
        return y
    else:
        y+np.random.randn(*y.shape) * noise_std



def y_l(x):
    return(0.6 *(x**4) - 0.3 * (x**3) - 3 * (x**2)- 1.2 * x)

def multi_fidelity_1d(x):
    if type(x) == torch.Tensor:
        input_copy = x.clone()
    elif type(x) == np.ndarray:
        # np.ndarray.flatten(input)
        input_copy = np.copy(x)
    y_list = []
    for X in input_copy:
        if X[-1] == 0.0:
            y_list.append(y_h(X[0]))
        elif X[-1] == 1.0:
            y_list.append(y_l(X[0]))
        else:
            raise ValueError('Wrong label, should be h or l1')
    return torch.tensor(np.hstack(y_list))


def data_1d (n_high, n_low):
    x_high=np.linspace(-2.5,3,n_high)
    x_low=np.linspace(-2.5,3,n_low)

    y_high= y_h(x_high)
    y_low=y_l(x_low)

    x_high=np.concatenate((x_high.reshape(-1,1), np.zeros(x_high.shape[0]).reshape(-1,1)), axis=-1)
    x_low=np.concatenate((x_low.reshape(-1,1), np.ones(x_low.shape[0]).reshape(-1,1)), axis=-1)

    x_full=torch.tensor(np.concatenate((x_high,x_low), axis=0))
    y_full=torch.tensor(np.concatenate((y_high,y_low), axis=0))

    return(x_full, y_full)




    





