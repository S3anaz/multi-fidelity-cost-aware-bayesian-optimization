import os
import torch
import pickle
from botorch.models.gp_regression_fidelity import SingleTaskMultiFidelityGP
from botorch.models.transforms.outcome import Standardize
from gpytorch.mlls.exact_marginal_log_likelihood import ExactMarginalLogLikelihood
from botorch import fit_gpytorch_model
from botorch.acquisition.cost_aware import InverseCostWeightedUtility
from botorch.acquisition import PosteriorMean
from botorch.acquisition.knowledge_gradient import qMultiFidelityKnowledgeGradient
from botorch.acquisition.fixed_feature import FixedFeatureAcquisitionFunction
from botorch.optim.optimize import optimize_acqf
from botorch.acquisition.utils import project_to_target_fidelity
from botorch.optim.optimize import optimize_acqf_mixed
import numpy as np 
from set_seed import set_seed
from sklearn.preprocessing import MinMaxScaler
from cost_model import FlexibleFidelityCostModel
from multi_fidelity_borehole import multi_fidelity_borehole
from Data_Gen import Borehole_MF

def run_MA2X_Bayesian(seed,num_high,num_low_1,num_low_2,num_low_3):

    tkwargs = {
        "dtype": torch.double,
        "device": torch.device("cuda" if torch.cuda.is_available() else "cpu"),
    }
    SMOKE_TEST = os.environ.get("SMOKE_TEST")
    data_fidelity=8

    target_fidelities = {8: 1.0}


    set_seed(seed)
    problem = lambda x: multi_fidelity_borehole(x).to(**tkwargs)
    fidelities = torch.tensor([0.5,0.75,1.0], **tkwargs)

    MIN = (100, 990, 700, 100, .05, 10, 1000, 6000,0)
    MAX = (1000, 1110, 820, 10000, .15, 500, 2000, 12000,1)
    bounds = torch.tensor([MIN, MAX], **tkwargs)


    X, y = Borehole_MF((num_high,num_low_1,num_low_2,num_low_3))

    train_x=X[0:-1,:]
    train_obj=y[0:-1]


    def initialize_model(train_x, train_obj):
        # define a surrogate model suited for a "training data"-like fidelity parameter
        # in dimension 6, as in [2]
        model = SingleTaskMultiFidelityGP(train_x, train_obj, outcome_transform=Standardize(m=1), data_fidelity=data_fidelity)
        mll = ExactMarginalLogLikelihood(model.likelihood, model)
        return mll, model


    


    # cost_model = AffineFidelityCostModel(fidelity_weights={8: 1.0}, fixed_cost=5.0)
    cost_model = FlexibleFidelityCostModel(values={'1.0':1000, '0.75':100, '0.5':10}, fixed_cost=0)
    cost_aware_utility = InverseCostWeightedUtility(cost_model=cost_model)


    def project(X):
        return project_to_target_fidelity(X=X, target_fidelities=target_fidelities)

    def get_mfkg(model):
        
        curr_val_acqf = FixedFeatureAcquisitionFunction(
            acq_function=PosteriorMean(model),
            d=9,
            columns=[8],
            values=[1],
        )
        
        _, current_value = optimize_acqf(
            acq_function=curr_val_acqf,
            bounds=bounds[:,:-1],
            q=1,
            num_restarts=10 if not SMOKE_TEST else 2,
            raw_samples=1024 if not SMOKE_TEST else 4,
            options={"batch_limit": 10, "maxiter": 200},
        )
            
        return qMultiFidelityKnowledgeGradient(
            model=model,
            num_fantasies=128 if not SMOKE_TEST else 2,
            current_value=current_value,
            cost_aware_utility=cost_aware_utility,
            project=project,
        )




    torch.set_printoptions(precision=3, sci_mode=False)

    NUM_RESTARTS = 5 if not SMOKE_TEST else 2
    RAW_SAMPLES = 128 if not SMOKE_TEST else 4
    BATCH_SIZE = 1


    def optimize_mfkg_and_get_observation(mfkg_acqf):
        """Optimizes MFKG and returns a new candidate, observation, and cost."""

        # generate new candidates
        candidates, _ = optimize_acqf_mixed(
            acq_function=mfkg_acqf,
            bounds=bounds,
            fixed_features_list=[{8: 0.50}, {8: 0.75}, {8: 1.0}],
            q=BATCH_SIZE,
            num_restarts=NUM_RESTARTS,
            raw_samples=RAW_SAMPLES,
            # batch_initial_conditions=X_init,
            options={"batch_limit": 5, "maxiter": 200},
        )

        # observe new values
        
        cost = cost_model(candidates).sum()
        new_x = candidates.detach()
        new_obj = problem(new_x).unsqueeze(-1)
        print(f"candidates:\n{new_x}\n")
        print(f"observations:\n{new_obj}\n\n")
        return new_x, new_obj, cost

    scaler = MinMaxScaler()
    scaler.fit(train_x[:,0:-1])
    train_x[:,0:-1]=torch.from_numpy(scaler.transform(train_x[:,0:-1]))
    train_x=torch.tensor(train_x)
    train_obj=torch.tensor(train_obj)

    cumulative_cost=cost_model(train_x).sum()
    cost_max=40000
    cumulative_cost_hist=[]
    recommended_y=[]
    bestf=[]
    Fidelity=[]
  
    def get_recommendation(model):
            rec_acqf = FixedFeatureAcquisitionFunction(
                acq_function=PosteriorMean(model),
                d=9,
                columns=[8],
                values=[1],
            )

            final_rec, _ = optimize_acqf(
                acq_function=rec_acqf,
                bounds=bounds[:,:-1],
                q=1,
                num_restarts=10,
                raw_samples=512,
                options={"batch_limit": 5, "maxiter": 200},
            )
            
            final_rec = rec_acqf._construct_X_full(final_rec)
            
            objective_value = problem(final_rec)
            # print(f"recommended point:\n{final_rec}\n\nobjective value:\n{objective_value}")
            return final_rec, objective_value

    i=0
    while cumulative_cost < cost_max:
            i+=1
            mll, model = initialize_model(train_x, train_obj)
            fit_gpytorch_model(mll)
            mfkg_acqf = get_mfkg(model)
            new_x, new_obj, cost = optimize_mfkg_and_get_observation(mfkg_acqf)
            new_x[:,0:-1]=torch.tensor(scaler.transform(new_x[:,0:-1]))
            train_x = torch.cat([train_x, new_x])
            train_obj = torch.cat([train_obj, new_obj])
            cumulative_cost += cost
            cumulative_cost_hist.append(cumulative_cost.clone().cpu().numpy())
            final_rec, final_y = get_recommendation(model)
            recommended_y.append(-1*final_y[0])
            bestf.append(min(recommended_y))
            print(f'best_f is {bestf[-1]}')
            Fidelity.append(new_x[0][-1])
            # if len(bestf)> 50:
            #     if np.var(bestf[-50:])<1e-6:
            #         break


    return np.array(bestf), np.array(cumulative_cost_hist),np.array(Fidelity)

if __name__ == '__main__':

    
    np.random.seed(123)
    random_seed = np.random.choice(range(0,1000), size=20, replace=False)
    output = {'best_f':[], 'cost':[], 'Fidelity':[]}
    itr = 0
    for seed in random_seed:
        itr += 1
        [bestf,cost,Fidelity] = \
            run_MA2X_Bayesian(seed = seed,num_high=5,num_low_1=5,num_low_2=50,num_low_3=1)
        print(f'***************** Random state: {itr} *****************')
        output['best_f'].append(bestf)
        output['cost'].append(cost)
        output['Fidelity'].append(Fidelity)
        
        
        
