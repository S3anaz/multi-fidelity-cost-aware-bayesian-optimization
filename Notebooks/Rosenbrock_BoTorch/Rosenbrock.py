from matplotlib import projections
import numpy as np
import matplotlib.pyplot as plt
import plotly.graph_objects as go
from mpl_toolkits import mplot3d
import torch
from scipy.io import savemat
from scipy.optimize import  minimize

plot_flag = 1

def rosen(x):
    x1 = x[0]
    x2 = x[1]
    return (1.0 - x1)**2 + 100.0 * ( x2 - x1**2 )**2 + -456.3 


def rosen_low(x):
    x1 = x[0]
    x2 = x[1]
    return rosen(x) - 10 * np.sin(0.1 * x1 + 5.0 * x2)



def find_min(x0):
    res0 = minimize(rosen, x0 = x0, method='Nelder-Mead', options={'gtol': 1e-8, 'disp': True})
    res1 = minimize(rosen_low, x0 = x0, method='Nelder-Mead', options={'gtol': 1e-8, 'disp': True})
    print(f'the minimum in rosen is {res0.fun} at {res0.x}')
    print(f'the minimum in rosen_low is {res1.fun} at {res1.x}')
    return res1

def multi_fidelity_rosen(x, negate=False, mapping=None):

    if mapping is not None:
        x[..., 1] = torch.tensor(list(map(lambda x: mapping[str(float(x))], x[..., 1]))).to(x)

    if type(x) == torch.Tensor:
        input_copy = x.clone()
    elif type(x) == np.ndarray:
        # np.ndarray.flatten(input)
        input_copy = np.copy(x)
    y_list = []
    for X in input_copy:
        if X[-1] == 1.0:
            y_list.append(rosen(X))
        elif X[-1] == torch.tensor(0.75):
            y_list.append(rosen_low(X))
        else:
            raise ValueError('Wrong label, should be h or l1')
    return -1*(torch.tensor(np.hstack(y_list))) if negate else (torch.tensor(np.hstack(y_list)))

def generate_rosen(low = 10, high = 10, plot_flag = 1, low_min = [1,1]):

    x = np.linspace(-2,2,100)
    y = np.linspace(-2,2,100)
    x1,x2 = np.meshgrid(x,y)

    z = rosen([x1,x2])
    z2 = rosen_low([x1, x2])

    x1_1d = x1.reshape(-1,1)
    x2_1d = x2.reshape(-1,1)
    z_1d = z.reshape(-1,1)
    z2_1d = z2.reshape(-1,1)

    data = np.zeros((2*len(x1_1d), 4))

    for i in range(len(x1_1d)):
        data[i,0] =  x1_1d[i]
        data[i,1] =  x2_1d[i]
        data[i,2] =  z_1d[i]
        data[i,3] =  0

    i += 1
    for j in range(len(x1_1d)):
        data[i+j,0] =  x1_1d[j]
        data[i+j,1] =  x2_1d[j]
        data[i+j,2] =  z2_1d[j]
        data[i+j,3] =  1

    
    if plot_flag:
        plt.figure()
        mycmap = plt.get_cmap('gist_earth')
        h = plt.contourf(x1, x2, z, cmap = mycmap)
        plt.scatter(1,1,color = 'red')
        plt.axis('scaled')
        plt.colorbar()

        plt.figure()
        mycmap = plt.get_cmap('gist_earth')
        h = plt.contourf(x1, x2, z2, cmap = mycmap)
        plt.axis('scaled')
        plt.scatter(low_min[0], low_min[1],color = 'red')
        plt.colorbar()


        fig = plt.figure()
        ax = plt.axes(projection = '3d')
        surf1 = ax.plot_surface(x1, x2, z, cmap = mycmap, alpha=1.0)
        surf2 = ax.plot_surface(x1, x2, z2, cmap='seismic', alpha = 1.0)
        fig.colorbar(surf1)
        plt.show()

        '''
        fig = go.Figure(data = go.Contour(x = x, y = y, z= z))
        fig.show()

        fig = go.Figure(data=[go.Surface(x=x1, y=x2, z=z)])
        fig.show()
        '''

    return data
    
if __name__ == '__main__':
    res_low = np.zeros((10,3))
    for i in range(10):
        initial = (np.random.rand(1,2) * 2 -1) * 2
        print(initial)
        out= find_min(initial)
        res_low[i,0:2] = out.x
        res_low[i,-1] = out.fun
    
    min = np.argmin([res_low[i,-1] for i in range(10)])
    print(f'min with many iterations is {res_low[min,-1]} at {res_low[min,0:2]}')
    data = generate_rosen(plot_flag = 1, low_min= res_low[min,0:2])