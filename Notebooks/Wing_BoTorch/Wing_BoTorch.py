import os
import torch

from botorch.utils.sampling import draw_sobol_samples
from multi_fidelity import  multi_fidelity_wing_value_BO
from sklearn.preprocessing import MinMaxScaler
import numpy as np
import time
import pickle
from set_seed import set_seed
from botorch import fit_gpytorch_model
from botorch.acquisition.cost_aware import InverseCostWeightedUtility
from botorch.acquisition import PosteriorMean
from botorch.acquisition.knowledge_gradient import qMultiFidelityKnowledgeGradient
from botorch.acquisition.fixed_feature import FixedFeatureAcquisitionFunction
from botorch.optim.optimize import optimize_acqf
from botorch.acquisition.utils import project_to_target_fidelity
from botorch.models.gp_regression_fidelity import SingleTaskMultiFidelityGP
from botorch.models.transforms.outcome import Standardize
from gpytorch.mlls.exact_marginal_log_likelihood import ExactMarginalLogLikelihood
from cost_model import FlexibleFidelityCostModel

def run_MA2X_Bayesian(seed,BO_initial_high,num_low_1,num_low_2,num_low_3):
    tkwargs = {
        "dtype": torch.double,
        "device": torch.device("cuda" if torch.cuda.is_available() else "cpu"),
    }
    SMOKE_TEST = os.environ.get("SMOKE_TEST")

    ##################

    n_high=BO_initial_high
    num_low_1=num_low_1
    num_low_2=num_low_2
    num_low_3=num_low_3
    l_bound = [150, 220, 6, -10, 16, 0.5, 0.08, 2.5, 1700, 0.025,0]
    u_bound = [200, 300, 10, 10, 45, 1, 0.18, 6, 2500, 0.08,1]
    bounds = torch.tensor([l_bound, u_bound], **tkwargs)
    target_fidelities = {10: 1.0}
    fidelities = torch.tensor([1.0, 0.96,0.83,0.49], **tkwargs)
    fixed_features_list=[{10: 1.0}, {10: 0.96}, {10: 0.83}, {10: 0.49}]
    

    set_seed(seed)
    problem = lambda x: multi_fidelity_wing_value_BO(x).to(**tkwargs)

    cost_model = FlexibleFidelityCostModel(values={'1.0':1000, '0.96':100,'0.83':10,'0.49':1}, fixed_cost=0)
    cost_aware_utility = InverseCostWeightedUtility(cost_model=cost_model)

    #################################
    def generate_initial_data(num_high,num_low_1,num_low_2,num_low_3,):
        # generate training data
        if num_low_1 ==0:
            train_x_high = draw_sobol_samples(bounds[:,:-1],n=num_high,q = 1, batch_shape= None).squeeze(1).to(**tkwargs)
            train_f_high = fidelities[torch.randint(1, (num_high, 1))]
            train_x_full = torch.cat((train_x_high, train_f_high), dim=1)
            train_obj = problem(train_x_full).unsqueeze(-1) 
        
        else:
            ###################################
            train_x_high = draw_sobol_samples(bounds[:,:-1],n=num_high,q = 1, batch_shape= None).squeeze(1).to(**tkwargs)
            train_f_high = fidelities[torch.randint(1, (num_high, 1))]
            train_x_full_high = torch.cat((train_x_high, train_f_high), dim=1)

            train_x_low_1 = draw_sobol_samples(bounds[:,:-1],n=num_low_1,q = 1, batch_shape= None).squeeze(1).to(**tkwargs)
            train_f_low_1 = fidelities[1]*torch.ones(num_low_1,1)
            train_x_full_low_1 = torch.cat((train_x_low_1, train_f_low_1), dim=1)

            train_x_low_2 = draw_sobol_samples(bounds[:,:-1],n=num_low_2,q = 1, batch_shape= None).squeeze(1).to(**tkwargs)
            train_f_low_2 = fidelities[2]*torch.ones(num_low_2,1)
            train_x_full_low_2 = torch.cat((train_x_low_2, train_f_low_2), dim=1)

            train_x_low_3 = draw_sobol_samples(bounds[:,:-1],n=num_low_3,q = 1, batch_shape= None).squeeze(1).to(**tkwargs)
            train_f_low_3 = fidelities[3]*torch.ones(num_low_3,1)
            train_x_full_low_3 = torch.cat((train_x_low_3, train_f_low_3), dim=1)

            train_x_full = torch.cat((train_x_full_high, train_x_full_low_1,train_x_full_low_2,train_x_full_low_3), dim=0)
            train_obj = problem(train_x_full).unsqueeze(-1) 




        return train_x_full, train_obj




    def initialize_model(train_x, train_obj):
        model = SingleTaskMultiFidelityGP(train_x, train_obj, outcome_transform=Standardize(m=1), data_fidelity=2)
        mll = ExactMarginalLogLikelihood(model.likelihood, model)
        return mll, model

    #########################
    

    def project(X):
        return project_to_target_fidelity(X=X, target_fidelities=target_fidelities)

    def get_mfkg(model):
        
        curr_val_acqf = FixedFeatureAcquisitionFunction(
            acq_function=PosteriorMean(model),
            d=11,
            columns=[10],
            values=[1],
        )
        
        _, current_value = optimize_acqf(
            acq_function=curr_val_acqf,
            bounds=bounds[:,:-1],
            q=1,
            num_restarts=10 if not SMOKE_TEST else 2,
            raw_samples=1024 if not SMOKE_TEST else 4,
            options={"batch_limit": 10, "maxiter": 200},
        )
            
        return qMultiFidelityKnowledgeGradient(
            model=model,
            num_fantasies=128 if not SMOKE_TEST else 2,
            current_value=current_value,
            cost_aware_utility=cost_aware_utility,
            project=project,
        )

    ################################

    from botorch.optim.optimize import optimize_acqf_mixed


    torch.set_printoptions(precision=3, sci_mode=False)

    NUM_RESTARTS = 5 if not SMOKE_TEST else 2
    RAW_SAMPLES = 128 if not SMOKE_TEST else 4
    BATCH_SIZE = 1


    def optimize_mfkg_and_get_observation(mfkg_acqf):
        """Optimizes MFKG and returns a new candidate, observation, and cost."""

        # generate new candidates
        candidates, _ = optimize_acqf_mixed(
            acq_function=mfkg_acqf,
            bounds=bounds,
            fixed_features_list=fixed_features_list,
            q=BATCH_SIZE,
            num_restarts=NUM_RESTARTS,
            raw_samples=RAW_SAMPLES,
            # batch_initial_conditions=X_init,
            options={"batch_limit": 5, "maxiter": 200},
        )

        # observe new values
        cost = cost_model(candidates).sum()
        new_x = candidates.detach()
        new_obj = problem(new_x).unsqueeze(-1)
        print(f"candidates:\n{new_x}\n")
        print(f"observations:\n{new_obj}\n\n")
        print(f'iteration{i}')
        return new_x, new_obj, cost


    ##############################
    scaler = MinMaxScaler()
    train_x, train_obj = generate_initial_data(n_high,num_low_1,num_low_2,num_low_3)
    scaler.fit(train_x[:,0:-1])
    train_x[:,0:-1]=torch.from_numpy(scaler.transform(train_x[:,0:-1]))
    ###############################

    def get_recommendation(model):
        rec_acqf = FixedFeatureAcquisitionFunction(
            acq_function=PosteriorMean(model),
            d=11,
            columns=[10],
            values=[1],
        )

        final_rec, _ = optimize_acqf(
            acq_function=rec_acqf,
            bounds=bounds[:,:-1],
            q=1,
            num_restarts=10,
            raw_samples=512,
            options={"batch_limit": 5, "maxiter": 200},
        )
        
        final_rec = rec_acqf._construct_X_full(final_rec)
        
        objective_value = problem(final_rec)
        # print(f"recommended point:\n{final_rec}\n\nobjective value:\n{objective_value}")
        return final_rec, objective_value



    cumulative_cost =cost_model(train_x).sum()
    N_ITER = 60 if not SMOKE_TEST else 1
    cumulative_cost_hist=[]
    best_x=[]
    bestf=[]
    recommended_y=[]
    Fidelity=[]
    cost_max=40000
    # for i in range(N_ITER):
    i=0
    while cumulative_cost < cost_max:
        i+=1
        mll, model = initialize_model(train_x, train_obj)
        fit_gpytorch_model(mll)
        mfkg_acqf = get_mfkg(model)
        new_x, new_obj, cost = optimize_mfkg_and_get_observation(mfkg_acqf)
        new_x[:,0:-1]=torch.tensor(scaler.transform(new_x[:,0:-1]))
        train_x = torch.cat([train_x, new_x])
        train_obj = torch.cat([train_obj, new_obj])
        cumulative_cost += cost
        cumulative_cost_hist.append(cumulative_cost.clone().cpu().numpy())
        final_rec, final_y = get_recommendation(model)
        recommended_y.append(final_y)
        bestf.append(max(recommended_y))
        Fidelity.append(new_x[0][-1])
        


    ##################################


    ############################
    final_rec = get_recommendation(model)
    print(f"\ntotal cost: {cumulative_cost}\n")

    return np.array(bestf), np.array(cumulative_cost_hist),np.array(Fidelity)


if __name__ == '__main__':

    t1 = time.time()
    np.random.seed(123)
    random_seed = np.random.choice(range(0,1000), size=20, replace=False)
    output = {'best_f':[], 'cost':[], 'Fidelity':[]}
    itr = 0
    for seed in random_seed:
        itr += 1
        [bestf,cost,Fidelity] = \
            run_MA2X_Bayesian(seed = seed, BO_initial_high = 5,num_low_1=5,num_low_2=10,num_low_3=50)
        print(f'***************** Random state: {itr} *****************')
        output['best_f'].append(bestf)
        output['cost'].append(cost)
        output['Fidelity'].append(Fidelity)
        
        print(f'total time is {time.time() - t1}')
